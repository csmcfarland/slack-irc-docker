#!/usr/bin/env bash
set -e

echo "generating config"
confd -onetime -backend env
echo "executing $@"
exec "$@"
