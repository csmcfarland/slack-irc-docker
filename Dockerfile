FROM node:14.4

COPY ./confd /etc/confd
COPY entrypoint.sh /usr/local/bin/
RUN npm install -g slack-irc && \
  curl -s -L https://github.com/kelseyhightower/confd/releases/download/v0.16.0/confd-0.16.0-linux-amd64 -o /usr/local/bin/confd && \
  chmod +x /usr/local/bin/confd && \
  chmod u+x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

CMD ["slack-irc", "-c", "/slack-irc.json"]
